import { _decorator, Component, Node, Sprite, Material, UITransform, EffectAsset } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('CircleCut')
export class CircleCut extends Component {

    @property([Sprite])
    private readonly imgs: Sprite[] = [];

    @property(EffectAsset)
    private readonly effect: EffectAsset = null;

    start() {
        // if (this.imgs.length > 0)
        //     this.imgs.forEach(img => this.updateMaterial(img));

        this.imgs.forEach(img=>console.log('材质列表：', img.materials));
    }

    private updateMaterial(img: Sprite) {
        if (!img || !this.effect) return;

        let mat = new Material();
        mat.initialize({ effectAsset: this.effect, defines: { 'USE_TEXTURE': true } });
        mat.name = this.effect.name
        img.setMaterial(mat, 1);

        const size = img.node.getComponent(UITransform)!.contentSize;
        const rate = size.width / size.height;
        img.getMaterial(1)!.setProperty('wh_ratio', rate);
    }

}


