"use strict";
/// <reference path='../Function.js' />

function main() {

    // 获取WebGL上下文
    const gl = getNewCanvasGLContent();
    if (!gl) return;
    gl.enable(gl.CULL_FACE);

    // 定义顶点着色器脚本文本，用于传递给着色器处理
    const vertexSharderSource = `
    // 声明接受顶点位置数据
    attribute vec2 a_position;
    // 着色器入口函数
    void main() {
        // gl_Position填入数据，数据类型vec4; (由vec2直接填入xy, 深度z=0.0, w=1.0)
        gl_Position= vec4(a_position, 0.0, 1.0);
    }
    `;
    // 根据定义的顶点着色器脚本文本，创建WebGL上可以使用的顶点着色器对象
    const vertexSharder = createShader(gl, gl.VERTEX_SHADER, vertexSharderSource);

    // 定义一个三角形顶点数据
    const posTri = [
        0.0, 1.0,
        -0.5, 0.0,
        0.5, -0.5
    ];
    // 定义顶点缓冲对象（一次性发送一批数据，减少传输数据用时）
    const vertexBuffer = gl.createBuffer();
    // 将顶点缓冲对象绑定到WebGL的ARRAY_BUFFER字段上；（不同字段不会出现覆盖)
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    // 为顶点缓冲对象填入数据，这里float型数据采用Float32数组类型以保证精度；
    // gl.STATIC_DRAW ：数据不会或几乎不会改变。
    // gl.DYNAMIC_DRAW：数据会被改变很多。
    // gl.STREAM_DRAW ：数据每次绘制时都会改变
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(posTri), gl.STATIC_DRAW);

    // 定义片元着色器脚本文本
    const fragmentSource = `
    // 声明所有浮点数精度为中精度
    precision mediump float;
    // 着色器入口函数
    void main() {
        // 将三角形颜色存入WebGL指定变量gl_FragColor, 这里简单的使用简单的纯色。
        // 新年了，用点红色喜庆！将RGBA值（0~255）归一化处理 (透明度Alpha这里设置成255,不透明)
        gl_FragColor = vec4(1,0,0,1);
    }
    `;
    // 根据定义的片元着色器脚本文本，创建WebGL上可以使用的片元着色器对象
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentSource);

    // 将配置好的顶点着色器和片元着色器，绑定到着色程序上, 并传输给WebGL
    const program = createProgram(gl, vertexSharder, fragmentShader);

    // 设置视口尺寸，将视口尺寸和画布尺寸同步，方便在屏幕影射时将NDC坐标转换到屏幕坐标
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    // 清除画布颜色，并设置画布颜色(用于区分页面背景和画布)；
    gl.clearColor(0.7, 0.7, 0.7, 1);
    // 清除颜色缓冲（防止意外的数据导致花屏）；
    gl.clear(gl.COLOR_BUFFER_BIT);

    // 启用指定的着色程序
    gl.useProgram(program);

    // 获取顶点位置属性在顶点着色器中的索引位置
    const attrLocatioIdx = gl.getAttribLocation(program, "a_position");
    // 每次操作GPU属性，需要先激活属性列表中对应的属性
    gl.enableVertexAttribArray(attrLocatioIdx);

    // 重新绑定顶点缓冲到ARRAY_BUFFER上，确保当前缓冲是所需要的顶点缓冲
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer)
    // 告诉WebGL如何获取获取参数
    gl.vertexAttribPointer(attrLocatioIdx, 2, gl.FLOAT, false, 0, 0);

    // 绘制三角形
    gl.drawArrays(gl.TRIANGLES, 0, 3);
}

// 执行JS的main函数
main();