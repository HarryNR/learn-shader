"use strict";

/**
 * 创建着色器对象
 * @param gl  WebGL上下文
 * @param type 着色器类型（顶点着色器、片段着色器）
 * @param source 着色器脚本文本
 * @returns 着色器对象(shader)
 */
function createShader(gl, type, source) {
    // 按照type(着色器类型)创建着色器
    let shader = gl.createShader(type);
    // 绑定脚本内容source
    gl.shaderSource(shader, source);
    // 编译配置好的着色器
    gl.compileShader(shader);
    // 获取编译后的状态
    const status = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    // 若编译成功，返回着色器对象shader
    if (status)
        return shader;
    // 否则，打印失败信息，方便查找问题
    console.log(gl.getShaderInfoLog(shader));
    // 删除失败的着色器对象
    gl.deleteShader(shader);
}

/**
 * 创建着色程序
 * @param gl WebGL上下文
 * @param vertexShader 顶点着色器对象
 * @param fragmentShader 片元着色器对象
 * @returns 着色程序（program）
 */
function createProgram(gl, vertexShader, fragmentShader) {
    // 创建着色器程序
    let program = gl.createProgram();
    // 程序对象加载顶点着色器对象
    gl.attachShader(program, vertexShader);
    // 程序对象加载片元着色器对象
    gl.attachShader(program, fragmentShader);
    // WebGL装载已经配置好的着色器程序
    gl.linkProgram(program);
    // 获取着色器编译结果
    const status = gl.getProgramParameter(program, gl.LINK_STATUS);
    // 若编译成功，返回着色器程序对象
    if (status)
        return program;
    // 打印失败信息，便于追溯问题
    console.log(gl.getProgramInfoLog(program));
    // 删除绑定失败的着色器程序对象
    gl.deleteProgram(program);
}

/**
 * 创建画布，设置尺寸，并返回WebGL对象
 * @returns gl 获取WebGL上下文(Context)
 */
function getNewCanvasGLContent() {
    // 创建画布
    const canvas = document.createElement("canvas");
    // 将新建的画布添加到网页内
    document.getElementsByTagName("body")[0].appendChild(canvas);
    // 定义画布尺寸
    canvas.width = 1440;
    canvas.height = 720;

    // 获取WebGL上下文（cocos使用的是三维渲染，类型;webgl/webgl2）
    const gl = canvas.getContext("webgl");
    // 判断当前设备是否支持WebGL渲染
    if (gl)
        return gl;
    // 若失败，打印错误信息
    console.log("当前设备不支持WebGL渲染！");
}
